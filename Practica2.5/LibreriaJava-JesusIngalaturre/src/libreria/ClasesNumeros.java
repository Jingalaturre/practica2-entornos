package libreria;

public class ClasesNumeros {
	
	public static boolean esEntero(String cadena) {
		for(int i = 0; i< cadena.length(); i++){
			if(cadena.charAt(i) >= 'A' && cadena.charAt(i) <= 'Z' ){
				return false;
			}
			if(cadena.charAt(i)>='a' && cadena.charAt(i)<='z' ){
				return false;
			}
			if(cadena.charAt(0) == '-' && cadena.length() == 1){
				return false;
			}
			
			if(i == 0 && cadena.charAt(i) != '-' && cadena.charAt(i) < '0' && cadena.charAt(i) > '9'){
				return false;
			}
			
			if((cadena.charAt(i) < '0' || cadena.charAt(i) > '9') && i != 0){
				return false;
			}
		}
		
		return true;
	}
	
	public static int numeroAleatorio (int comienzo, int fin){
		
		
		 int numRandom = (int) (Math.random()* (fin - comienzo+1))+ comienzo;
		 
		 
		 return numRandom;
	 }
	
	public static int suma (int num1, int num2){
		int numeroSuma = num1+num2;
		return numeroSuma;


	}
	
	public static int potencia(int base, int exponente){
		int resultado = 1;
		
		for(int i = 0; i < exponente; i++){
			resultado = resultado * base;
		}
		return resultado;
	}
	
	public static void mostrarTabla(int numero){
		for(int i =1 ; i<= 10 ; i++){
			System.out.println(i + " x " + numero + " = " + (i*numero));
		}
	}
	
}
