﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaPruebasVS
{
    public class Program
    {
        static void Main(string[] args)
        {
           
            LibreriaVS.Class2.abecedario();
            Console.WriteLine("Abecedario contador bucle");
            Console.WriteLine("Veces se repite un caracter");
            LibreriaVS.Class2.contardorCaracteres("a","asdfa");
            Console.WriteLine("Numero es o no Entero");
            Console.WriteLine(LibreriaVS.Class1.esEntero("8"));
            Console.WriteLine("Invierte la cadena escrita");
            Console.WriteLine(LibreriaVS.Class2.invertirCadena("asdf"));
            Console.WriteLine("Muestra tabla multiplicar de un numero");
            LibreriaVS.Class1.mostrarTabla(9);
            Console.WriteLine("Muestra numero aleatorio entre dos numeros");
            LibreriaVS.Class1.numeroAleatorio(6, 12);
            Console.WriteLine("Te dice cuantas palabras has escrito");
            LibreriaVS.Class2.palabrasSeparadas("wacha wacha wacha");
            Console.WriteLine("Porcentaje de cada vocal en la cadena");
            LibreriaVS.Class2.porcentajeVocales("cadena Leida");
            Console.WriteLine("Potencia de un numero elevado a otro");
            Console.WriteLine(LibreriaVS.Class1.potencia(2, 3));
            Console.WriteLine("Suma de dos numeros");
            Console.WriteLine(LibreriaVS.Class1.suma(2, 2));
            Console.WriteLine("Pulse una tecla para terminar...");
            Console.ReadKey();
        }
    }
}
