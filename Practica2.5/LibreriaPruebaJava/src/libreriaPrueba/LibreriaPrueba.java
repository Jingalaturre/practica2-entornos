package libreriaPrueba;

import libreria.ClasesNumeros;
import libreria.ClasesString;

public class LibreriaPrueba {

	public static void main(String[] args) {
		
		System.out.println(ClasesNumeros.esEntero("asdf"));
		
		ClasesNumeros.mostrarTabla(5);
		
		System.out.println("Numero aleatorio entre dos numeros " + ClasesNumeros.numeroAleatorio(5, 12));
		
		System.out.println("Potencias:(2e3) "+ClasesNumeros.potencia(2, 3));
		
		System.out.println("Suma 2+2 "+ ClasesNumeros.suma(2, 2));
		
		System.out.println("Abecedario Bucle contador");
		ClasesString.abecedario();
		
		ClasesString.contardorCaracteres("a", "anorexia");
		
		ClasesString.invertirCadena("hassan");
		ClasesString.palabrasSeparadas("Hola que miedo");
		ClasesString.porcentajeVocales("guau");
		
		
		
	}

}
