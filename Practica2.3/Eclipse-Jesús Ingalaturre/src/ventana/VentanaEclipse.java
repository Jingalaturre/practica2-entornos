package ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JEditorPane;
import javax.swing.ImageIcon;
import javax.swing.JScrollBar;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import java.awt.Toolkit;
import javax.swing.JTabbedPane;

/**
 * 
 * @author Jesus Ingalaturre
 * @since 
 *
 */
public class VentanaEclipse extends JFrame {

	private JPanel contentPane;
	private JTextField txtEscribeUnTitulo;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\JESUS\\Desktop\\Word-2-icon.png"));
		setTitle("WORD SUPER HACENDADO");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 681, 472);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivos = new JMenu("Archivos");
		menuBar.add(mnArchivos);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mntmNuevo.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/javax/swing/plaf/metal/icons/ocean/newFolder.gif")));
		mnArchivos.add(mntmNuevo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mntmAbrir.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/javax/swing/plaf/metal/icons/ocean/file.gif")));
		mnArchivos.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mntmGuardar.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/java/swing/plaf/windows/icons/FloppyDrive.gif")));
		mnArchivos.add(mntmGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como");
		mntmGuardarComo.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/javax/swing/plaf/metal/icons/ocean/floppy.gif")));
		mnArchivos.add(mntmGuardarComo);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mntmCopiar.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/javafx/scene/web/skin/Cut_16x16_JFX.png")));
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mntmPegar.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		mnEditar.add(mntmPegar);
		
		JMenuItem mntmHacer = new JMenuItem("Hacer");
		mntmHacer.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/javafx/scene/web/skin/Redo_16x16_JFX.png")));
		mnEditar.add(mntmHacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mntmRehacer.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/javafx/scene/web/skin/Undo_16x16_JFX.png")));
		mnEditar.add(mntmRehacer);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmBuscarEnInternet = new JMenuItem("Buscar en Internet");
		mntmBuscarEnInternet.setIcon(null);
		mnAyuda.add(mntmBuscarEnInternet);
		
		JMenuItem mntmTerminosYCondiciones = new JMenuItem("Terminos y Condiciones");
		mnAyuda.add(mntmTerminosYCondiciones);
		
		JMenuItem mntmSobreLaApp = new JMenuItem("Sobre la app");
		mntmSobreLaApp.setIcon(new ImageIcon(VentanaEclipse.class.getResource("/com/sun/javafx/scene/control/skin/caspian/dialog-more-details.png")));
		mnAyuda.add(mntmSobreLaApp);
		
		JMenuItem mntmPortearProblema = new JMenuItem("Portear problema");
		mnAyuda.add(mntmPortearProblema);
		
		JMenu mnNotificaciones = new JMenu("Notificaciones");
		menuBar.add(mnNotificaciones);
		
		JMenuItem mntmNoTienesNotificaciones = new JMenuItem("No tienes notificaciones");
		mnNotificaciones.add(mntmNoTienesNotificaciones);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setForeground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtEscribeUnTitulo = new JTextField();
		txtEscribeUnTitulo.setBounds(313, 51, 299, 29);
		txtEscribeUnTitulo.setText("Escribe un titulo");
		contentPane.add(txtEscribeUnTitulo);
		txtEscribeUnTitulo.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setForeground(Color.BLACK);
		comboBox.setBackground(Color.LIGHT_GRAY);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Arial", "Calibri", "Aharoni", "Time Romans", "Penguins", "Castellar"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(45, 15, 64, 29);
		contentPane.add(comboBox);
		
		JSlider slider = new JSlider();
		slider.setBounds(460, 387, 192, 14);
		contentPane.add(slider);
		
		JLabel lblNewLabel = new JLabel("Zoom");
		lblNewLabel.setBounds(404, 387, 46, 14);
		contentPane.add(lblNewLabel);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(0, 15, 46, 29);
		contentPane.add(spinner);
		
		JLabel lblTipoLetra = new JLabel("Tipo Letra");
		lblTipoLetra.setBounds(46, 0, 61, 14);
		contentPane.add(lblTipoLetra);
		
		JLabel lblLetra = new JLabel("  Letra");
		lblLetra.setBounds(0, 0, 46, 14);
		contentPane.add(lblLetra);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBackground(Color.WHITE);
		scrollBar.setBounds(612, 80, 17, 265);
		contentPane.add(scrollBar);
		
		JRadioButton rdbtnLetraEnNegrita = new JRadioButton("Letra en Negrita");
		rdbtnLetraEnNegrita.setBackground(Color.LIGHT_GRAY);
		rdbtnLetraEnNegrita.setBounds(109, 21, 112, 23);
		contentPane.add(rdbtnLetraEnNegrita);
		
		JRadioButton rdbtnLetraEnCursiva = new JRadioButton("Letra en Cursiva");
		rdbtnLetraEnCursiva.setBackground(Color.LIGHT_GRAY);
		rdbtnLetraEnCursiva.setBounds(218, 21, 109, 23);
		contentPane.add(rdbtnLetraEnCursiva);
		
		JButton Clean = new JButton("Clean");
		Clean.setBackground(Color.LIGHT_GRAY);
		Clean.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		Clean.setBounds(326, 0, 339, 44);
		contentPane.add(Clean);
		
		JCheckBox chckbxModoDiseo = new JCheckBox("Modo dise\u00F1o");
		chckbxModoDiseo.setBackground(Color.LIGHT_GRAY);
		chckbxModoDiseo.setBounds(109, -2, 112, 23);
		contentPane.add(chckbxModoDiseo);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.LIGHT_GRAY);
		textField_1.setBounds(-3, -3, 112, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnInsertarTabla = new JButton("Insertar Tabla");
		btnInsertarTabla.setBackground(Color.LIGHT_GRAY);
		btnInsertarTabla.setBounds(220, 0, 109, 23);
		contentPane.add(btnInsertarTabla);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(313, 80, 299, 265);
		contentPane.add(textPane);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 51, 261, 16);
		contentPane.add(toolBar);
		
		JButton btnAdaf = new JButton("Alinear");
		toolBar.add(btnAdaf);
		
		JButton btnIzquierda = new JButton("Izquierda");
		toolBar.add(btnIzquierda);
		
		JButton btnCentrado = new JButton("Centrado");
		toolBar.add(btnCentrado);
		
		JButton btnDerecha = new JButton("Derecha");
		toolBar.add(btnDerecha);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 90, 184, 216);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Revisar", null, panel, null);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Otrograf\u00EDa");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(0, 11, 190, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Sinonimos");
		btnNewButton_1.setBounds(0, 45, 190, 23);
		panel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Traducci\u00F3n");
		btnNewButton_2.setBounds(0, 113, 190, 23);
		panel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Antonimos");
		btnNewButton_3.setBounds(0, 79, 190, 23);
		panel.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Gram\u00E1tica");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4.setBounds(0, 147, 190, 23);
		panel.add(btnNewButton_4);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Impresi\u00F3n", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnImprimir = new JButton("Imprimir");
		btnImprimir.setBounds(0, 11, 190, 23);
		panel_2.add(btnImprimir);
		
		JButton btnNewButton_5 = new JButton("Impresi\u00F3n r\u00E1pida");
		btnNewButton_5.setBounds(0, 71, 190, 23);
		panel_2.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("Vista Preliminar");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_6.setBounds(0, 132, 190, 23);
		panel_2.add(btnNewButton_6);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Vista", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnNewButton_7 = new JButton("Dise\u00F1o de Impresi\u00F3n");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_7.setBounds(0, 11, 179, 23);
		panel_1.add(btnNewButton_7);
		
		JButton btnPantallaCompleta = new JButton("Pantalla Completa");
		btnPantallaCompleta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPantallaCompleta.setBounds(0, 62, 179, 23);
		panel_1.add(btnPantallaCompleta);
		
		JButton btnNewButton_8 = new JButton("Esquema");
		btnNewButton_8.setBounds(0, 117, 179, 23);
		panel_1.add(btnNewButton_8);
		
		JScrollBar scrollBar_1 = new JScrollBar();
		scrollBar_1.setOrientation(JScrollBar.HORIZONTAL);
		scrollBar_1.setBounds(314, 344, 315, 14);
		contentPane.add(scrollBar_1);
	}
}
