package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qu� realiza este programa
		 */
		
		
		/*
		 * Mediante escaner te pide un numero que sera numeroLeido.
		 * entra en el bucle donde se le da valor a i = 1
		 * Entonces si el numeroLeido entre i , su resto es cero.
		 * cantidadDivisores incrementara en 1.
		 * Este proceso se repetira un numero de veces similar a numeroLeido.
		 * Si al finalizar el numero de "cantidadDivisores" es mayor que dos, el programa imprimira en la consola : "No lo es"
		 * Si en cambio "cantidadDivisores" no es mayor que dos, el programa imprimira en la consola ; "Si lo es". 
		 * Despu�s de que se muestre una de las dos opciones terminara el programa.
		 * 
		 * Este ejercicio es una muestra de una forma de sacar los numeros primos pero no la �nica forma de hallarlos.
		 */
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();

	}

}
