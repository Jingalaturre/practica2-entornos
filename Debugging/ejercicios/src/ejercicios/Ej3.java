package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		/*
		 * No hay posible correci�n ya que el caracter 27 es ilegible en eclipse y 
		 * el ejercicio esta bien, podr�a arreglarse cambiando el caracter a uno legible.
		 * 
		 */
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
