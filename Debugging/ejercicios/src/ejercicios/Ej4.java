package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucci�n y avanzar
		 * instrucci�n a instrucci�n (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		double numeroLeido;
		double resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		/*El error como en el primero es el bucle que en este caso hace una divisi�n de m�s
		 * una soluci�n sencilla ser�a quitar el igual , tambi�n para darle m�s precisi�n al ejercicio
		 * podriamos poner las variables como doubles para que salgan los decimales.
		 * 
		 * 
		 */
		for(double i = numeroLeido; i >0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();

	}

}
