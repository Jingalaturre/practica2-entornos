﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_JesúsIngalaturre
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("Pasa de gramo a:"
                    + "\n 1 Kilogramo "
                    + "\n 2 Hectogramos"
                    + "\n 3 Decagramos"
                    + "\n 4 Decigramos"
                    + "\n 5 Centigramos"
                    + "\n 6 Miligramos");
            Console.WriteLine("Introduce opcion");
            String opcion1 = Console.ReadLine();
            int opcion = int.Parse(opcion1);
            Console.WriteLine("Introduce número");
            String num1 = Console.ReadLine();
            int num = int.Parse(num1);
            
            switch (opcion)
            {
                case 1:
                    kilogramo(num);
                    break;
                case 2:
                    hectogramos(num);
                    break;
                case 3:
                    decagramos(num);
                    break;
                case 4:
                    decigramos(num);
                    break;
                case 5:
                    centigramos(num);
                    break;
                case 6:
                    miligramos(num);
                    break;
                
            }
            Console.WriteLine("Pulse una tecla para terminar...");
            Console.ReadKey();
        }


        public static void miligramos(int num)
        {
            Console.WriteLine(num * 1000);

        }


        public static void centigramos(int num)
        {
            Console.WriteLine(num * 100);

        }


        public static void decigramos(int num)
        {
            Console.WriteLine(num * 10);

        }


        public static void decagramos(int num)
        {
            Console.WriteLine(num / 10);

        }


        public static void hectogramos(int num)
        {
            Console.WriteLine(num / 100);

        }


        public static void kilogramo(int num)
        {
            Console.WriteLine(num / 1000);

        }


    }



}
    

