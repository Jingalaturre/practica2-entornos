package consolaJava;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Pasa de gramo a:"
				+ "\n 1 Kilogramo "
				+ "\n 2 Hectogramos"
				+ "\n 3 Decagramos"
				+ "\n 4 Decigramos"
				+ "\n 5 Centigramos"
				+ "\n 6 Miligramos");
		System.out.println("Introduce opcion"  );
		int opcion = lector.nextInt();
		System.out.println("Introduce n�mero");
		int num = lector.nextInt();
		switch(opcion){
		case 1:
			kilogramo(num);
			break;
		case 2:
			hectogramos(num);
			break;
		case 3:
			decagramos(num);
			break;
		case 4:
			decigramos(num);
			break;
		case 5:
			centigramos(num);
			break;
		case 6:
			miligramos(num);
			break;
		default:
			System.out.println("Caso incorrecto");
		}
		lector.close();
	}
	
	
	public static void miligramos(int num) {
		System.out.println(num*1000);
		
	}


	public static void centigramos(int num) {
		System.out.println(num*100);
		
	}


	public static void decigramos(int num) {
		System.out.println(num*10);
		
	}


	public static void decagramos(int num) {
		System.out.println(num/10);
		
	}


	public static void hectogramos(int num) {
		System.out.println(num/100);
		
	}
	 

	public static void kilogramo(int num) {
		System.out.println(num/1000);
		
	}

	
	}


